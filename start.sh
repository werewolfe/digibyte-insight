#!/bin/bash

source /root/.bashrc
litecoind
until litecoin-cli getblockchaininfo > /dev/null 2>&1
do
    echo Waiting for litecoin wallet loading. It will take several minutes.
    sleep 30
done
block=$(litecoin-cli getblockchaininfo 2> /dev/null | jq '.blocks')
while [ $block -eq 0 ]; do
   echo Waiting litecoin to start syncing for some time first.
   sleep 30
   block=$(litecoin-cli getblockchaininfo 2> /dev/null | jq '.blocks')
done

header=$(litecoin-cli getblockchaininfo 2> /dev/null | jq '.headers')
echo "Now need for wallet to complete syncing first."
while [ $block -lt $header ]; do
  percent=$(( $block * 100 / $header ));
  echo "Fetching $block blocks of $header headers. $percent %";
  sleep 5;
  block=$(litecoin-cli getblockchaininfo 2> /dev/null | jq '.blocks')
  header=$(litecoin-cli getblockchaininfo 2> /dev/null | jq '.headers')
done

echo "Optimized TX fee. Done."
litecoin-cli settxfee 0
insightdb=$(shopt -s nullglob dotglob; echo /root/.insight/*)
if (( ${#insightdb} )); then
   cd /root/insight-api
   node insight.js
else
   echo "No insight database is found. Start syncing first. It may take 10-12 hours."
   echo "source /root/.bashrc && /root/insight-api/util/sync.js -v --rpc"
   source /root/.bashrc && /root/insight-api/util/sync.js -v --rpc
fi
