FROM utarn/devcentos
USER root
WORKDIR /root
ARG USERNAME=user
ARG PASSWORD=pass
COPY start.sh .
COPY restore.sh .
COPY litecoin.tar.gz .
#RUN curl -Llo /litecoin.tar.gz https://download.litecoin.org/litecoin-0.17.1/linux/litecoin-0.17.1-x86_64-linux-gnu.tar.gz
RUN chmod +x /root/start.sh \
    && tar xvfz litecoin.tar.gz  \
    && cd litecoin-0.17.1 \
    && /usr/bin/cp -r * /usr \
    && cd .. \
    && /usr/bin/rm -rf litecoin-0.17.1 \
    && /usr/bin/rm litecoin.tar.gz \
    && mkdir -vp .litecoin

VOLUME /root/.insight
VOLUME /root/.litecoin
EXPOSE 14022
EXPOSE 12024
EXPOSE 3000

RUN echo -e "export INSIGHT_NETWORK='livenet'\n\
export BITCOIND_DATADIR=~/.litecoin/\n\
export INSIGHT_FORCE_RPC_SYNC=1\n\
export INSIGHT_PUBLIC_PATH=public\n\
export BITCOIND_USER=${USERNAME}\n\
export BITCOIND_PASS=${PASSWORD}\n" >> /root/.bashrc
RUN echo -e "server=1\n\
maxconnections=300\n\
rpcallowip=0.0.0.0/0.0.0.0\n\
server=1\n\
daemon=1\n\
rpcuser=${USERNAME}\n\
rpcpassword=${PASSWORD}\n\
txindex=1\n" > /root/.litecoin/litecoin.conf

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash \
	&& source /root/.bashrc \
	&& nvm install v0.10.48 \
	&& git clone https://github.com/DigiByte-Core/insight \
	&& git clone https://github.com/DigiByte-Core/insight-api \
	&& cd insight-api \
	&& npm install \
	&& ln -s ../insight/public/
    
CMD /root/start.sh
